﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Extensions.Polling;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace school_courses
{
    class Program
    {
        private static string MyBotToken = "2065663237:AAFcpNUX8rKAZ2vKNx1hTfASy2cOUGYFu5o";


        static async Task Main(string[] args)
        {
            var botClient = new TelegramBotClient($"{MyBotToken}");
            var me = await botClient.GetMeAsync();
            Console.WriteLine(
                $"Hello, World! I am user {me.Id} and my name is {me.FirstName}."
                );
            using var cts = new CancellationTokenSource();

            // StartReceiving does not block the caller thread. Receiving is done on the ThreadPool.
            botClient.StartReceiving(
            new DefaultUpdateHandler(HandleUpdateAsync, HandleErrorAsync),
            cts.Token);

            Console.WriteLine($"Start listening for @{me.Username}");
            Console.ReadLine();

            // Send cancellation request to stop bot
            cts.Cancel();
        }

        public static Task HandleErrorAsync(ITelegramBotClient botClient, Exception exception, CancellationToken cancellationToken)
        {
            var ErrorMessage = exception switch
            {
                ApiRequestException apiRequestException => $"Telegram API Error:\n[{apiRequestException.ErrorCode}]\n{apiRequestException.Message}",
                _ => exception.ToString()
            };

            Console.WriteLine(ErrorMessage);
            return Task.CompletedTask;
        }

        public static async Task HandleUpdateAsync(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
        {
            if (update.Type != UpdateType.Message)
                return;
            if (update.Message.Type != MessageType.Text)
                return;

            var chatId = update.Message.Chat.Id;
            var inlineKeyboard = new InlineKeyboardMarkup(new[]
            {
                new[]{ InlineKeyboardButton.WithUrl(text: "Instagram", url: "https://www.instagram.com/jhokipery/")},
                new[]{ InlineKeyboardButton.WithUrl(text: "Vk", url: "https://vk.com/jhokipery")},
                new[]{ InlineKeyboardButton.WithUrl(text: "Vk", url: "https://vk.com/jhokipery") }
            });
            Message message = await botClient.SendTextMessageAsync(
                chatId: chatId, // or a chat id: 123456789
                text: "Я в соц сетях",
                parseMode: ParseMode.Markdown,
                disableNotification: true,
                replyToMessageId: update.Message.MessageId,
                replyMarkup: inlineKeyboard
            );
        }
    }
}
